package com.azhu12138.azhublog.constants;

/**
 * @author zhuliang
 * @date 2021/9/28 16:21
 */
public enum ArticleType {

    ORIGINAL("原创"),
    REPRINTED("转载");

    public String desc;

    ArticleType(String desc) {
        this.desc = desc;
    }
}
