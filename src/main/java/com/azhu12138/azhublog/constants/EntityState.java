package com.azhu12138.azhublog.constants;

/**
 * @author zhuliang
 * @date 2021/9/28 15:43
 */
public enum EntityState {
    NOT_ENABLED("未启用"),
    ENABLED("已启用"),
    DISABLED("已停用"),
    OBSOLETE("已废弃"),
    DELETED("已删除");

    public String desc;

    EntityState(String desc) {
        this.desc = desc;
    }

    public boolean canBeModified() {
        return !this.equals(OBSOLETE) && !this.equals(DELETED);
    }

}
