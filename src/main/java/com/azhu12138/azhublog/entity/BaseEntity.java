package com.azhu12138.azhublog.entity;

import com.azhu12138.azhublog.constants.EntityState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zhuliang
 * @date 2021/9/28 15:28
 */
@Data
@AllArgsConstructor
public abstract class BaseEntity implements Serializable {

    protected String pId; // uuid
    protected String pName; // 名称
    protected Date pCreateDatetime = new Date(); // 创建时间
    protected Date pUpdateDatetime = new Date(); // 修改时间
    protected EntityState pState; // 状态
    protected Integer pRank; // 排序值
    protected Integer pVersion; // 版本号
    protected String pRemark; // 备注

    public BaseEntity() {

    }

}
