package com.azhu12138.azhublog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AzhublogApplication {

    public static void main(String[] args) {
        SpringApplication.run(AzhublogApplication.class, args);
    }

}
