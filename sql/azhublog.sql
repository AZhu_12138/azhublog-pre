
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for azhublog_article
-- ----------------------------
DROP TABLE IF EXISTS `azhublog_article`;
CREATE TABLE `azhublog_article` (
      `p_id` varchar(50) NOT NULL COMMENT 'uuid' ,
      `p_name` varchar(255) DEFAULT NULL COMMENT '名称',
      `p_create_datetime` datetime DEFAULT NULL COMMENT '创建时间',
      `p_update_datetime` datetime DEFAULT NULL COMMENT '修改时间',
      `p_state` varchar(255) DEFAULT NULL COMMENT '状态',
      `p_rank` int(11) DEFAULT NULL COMMENT '排序值',
      `p_version` int(11) DEFAULT NULL COMMENT '版本号',
      `p_remark` text DEFAULT NULL COMMENT '备注',

      `user_id` varchar(50) DEFAULT NULL COMMENT '发表的用户uuid',

      `article_title` varchar(255) DEFAULT NULL COMMENT '文章标题',
      `article_summary` varchar(255) DEFAULT NULL COMMENT '文章摘要',
      `article_thumbnail` varchar(255) DEFAULT NULL COMMENT '略缩图',
      `article_content_html` longtext COMMENT '文章内容html格式',
      `article_content_md` longtext COMMENT '文章内容Markdown格式',
      `article_type` varchar(255) DEFAULT NULL COMMENT '文章类型',
      `article_post` varchar(255) DEFAULT NULL COMMENT 'post文章 page页面',
      `article_comment` int(11) DEFAULT NULL COMMENT '是否开启评论',
      `article_url` varchar(255) DEFAULT NULL COMMENT '文章路径',
      `article_views` bigint(20) DEFAULT '0' COMMENT '访问量',

      PRIMARY KEY (`p_id`) USING BTREE,
      UNIQUE KEY `AZHUBLOG_ARTICLE_URL` (`article_url`) USING BTREE,
      KEY `AZHUBLOG_ARTICLE_USERID` (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- ----------------------------
-- Table structure for azhublog_article_category
-- ----------------------------
DROP TABLE IF EXISTS `azhublog_article_category`;
CREATE TABLE `azhublog_article_category` (
       `p_id` varchar(50) NOT NULL COMMENT 'uuid' ,
       `p_name` varchar(255) DEFAULT NULL COMMENT '名称',
       `p_create_datetime` datetime DEFAULT NULL COMMENT '创建时间',
       `p_update_datetime` datetime DEFAULT NULL COMMENT '修改时间',
       `p_state` varchar(255) DEFAULT NULL COMMENT '状态',
       `p_rank` int(11) DEFAULT NULL COMMENT '排序值',
       `p_version` int(11) DEFAULT NULL COMMENT '版本号',
       `p_remark` text DEFAULT NULL COMMENT '备注',

       `article_id` varchar(50) NOT NULL COMMENT '文章uuid',
       `category_id` varchar(50) NOT NULL COMMENT '分类uuid',
       KEY `AZHUBLOG_ARTILE_ID` (`article_id`) USING BTREE,
       KEY `AZHUBLOG_ARTILE_CATEGORY_ID` (`category_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- ----------------------------
-- Table structure for azhublog_article_tag
-- ----------------------------
DROP TABLE IF EXISTS `azhublog_article_tag`;
CREATE TABLE `azhublog_article_tag` (
      `p_id` varchar(50) NOT NULL COMMENT 'uuid' ,
      `p_name` varchar(255) DEFAULT NULL COMMENT '名称',
      `p_create_datetime` datetime DEFAULT NULL COMMENT '创建时间',
      `p_update_datetime` datetime DEFAULT NULL COMMENT '修改时间',
      `p_state` varchar(255) DEFAULT NULL COMMENT '状态',
      `p_rank` int(11) DEFAULT NULL COMMENT '排序值',
      `p_version` int(11) DEFAULT NULL COMMENT '版本号',
      `p_remark` text DEFAULT NULL COMMENT '备注',

      `article_id` varchar(50) NOT NULL COMMENT '文章uuid',
      `tag_id` varchar(50) NOT NULL COMMENT '标签uuid',
      KEY `AZHUBLOG_ARTILE_ID` (`article_id`) USING BTREE,
      KEY `AZHUBLOG_ARTILE_TAG_ID` (`tag_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- ----------------------------
-- Table structure for azhublog_attachment
-- ----------------------------
DROP TABLE IF EXISTS `azhublog_attachment`;
CREATE TABLE `azhublog_attachment` (
       `p_id` varchar(50) NOT NULL COMMENT 'uuid' ,
       `p_name` varchar(255) DEFAULT NULL COMMENT '名称',
       `p_create_datetime` datetime DEFAULT NULL COMMENT '创建时间',
       `p_update_datetime` datetime DEFAULT NULL COMMENT '修改时间',
       `p_state` varchar(255) DEFAULT NULL COMMENT '状态',
       `p_rank` int(11) DEFAULT NULL COMMENT '排序值',
       `p_version` int(11) DEFAULT NULL COMMENT '版本号',
       `p_remark` text DEFAULT NULL COMMENT '备注',

        `picture_name` varchar(255) DEFAULT NULL COMMENT '图片名称',
        `picture_path` varchar(255) DEFAULT NULL COMMENT '图片路径',
        `picture_small_path` varchar(255) DEFAULT NULL COMMENT '略缩图路径',
        `picture_type` varchar(255) DEFAULT NULL COMMENT '图片类型',
        `picture_size` varchar(255) DEFAULT NULL COMMENT '文件大小',
        `picture_suffix` varchar(255) DEFAULT NULL COMMENT '后缀',
        `picture_wh` varchar(255) DEFAULT NULL COMMENT '尺寸',
        PRIMARY KEY (`p_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- ----------------------------
-- Table structure for azhublog_category
-- ----------------------------
DROP TABLE IF EXISTS `azhublog_category`;
CREATE TABLE `azhublog_category` (
     `p_id` varchar(50) NOT NULL COMMENT 'uuid' ,
     `p_name` varchar(255) DEFAULT NULL COMMENT '名称',
     `p_create_datetime` datetime DEFAULT NULL COMMENT '创建时间',
     `p_update_datetime` datetime DEFAULT NULL COMMENT '修改时间',
     `p_state` varchar(255) DEFAULT NULL COMMENT '状态',
     `p_rank` int(11) DEFAULT NULL COMMENT '排序值',
     `p_version` int(11) DEFAULT NULL COMMENT '版本号',
     `p_remark` text DEFAULT NULL COMMENT '备注',

    `category_name` varchar(255) DEFAULT NULL COMMENT '分类名称',
    `category_url` varchar(255) DEFAULT NULL COMMENT '分类路径',
    `category_describe` varchar(255) DEFAULT NULL COMMENT '描述',
     PRIMARY KEY (`p_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- ----------------------------
-- Table structure for azhublog_link
-- ----------------------------
DROP TABLE IF EXISTS `azhublog_link`;
CREATE TABLE `azhublog_link` (
     `p_id` varchar(50) NOT NULL COMMENT 'uuid' ,
     `p_name` varchar(255) DEFAULT NULL COMMENT '名称',
     `p_create_datetime` datetime DEFAULT NULL COMMENT '创建时间',
     `p_update_datetime` datetime DEFAULT NULL COMMENT '修改时间',
     `p_state` varchar(255) DEFAULT NULL COMMENT '状态',
     `p_rank` int(11) DEFAULT NULL COMMENT '排序值',
     `p_version` int(11) DEFAULT NULL COMMENT '版本号',
     `p_remark` text DEFAULT NULL COMMENT '备注',

   `link_name` varchar(255) DEFAULT NULL COMMENT '名称',
   `link_url` varchar(255) DEFAULT NULL COMMENT '路径',
   `link_logo` varchar(255) DEFAULT NULL COMMENT '链接logo',
   `link_describe` varchar(255) DEFAULT NULL COMMENT '描述',
     PRIMARY KEY (`p_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- ----------------------------
-- Table structure for azhublog_logs
-- ----------------------------
DROP TABLE IF EXISTS `azhublog_logs`;
CREATE TABLE `azhublog_logs` (
     `p_id` varchar(50) NOT NULL COMMENT 'uuid' ,
     `p_name` varchar(255) DEFAULT NULL COMMENT '名称',
     `p_create_datetime` datetime DEFAULT NULL COMMENT '创建时间',
     `p_update_datetime` datetime DEFAULT NULL COMMENT '修改时间',
     `p_state` varchar(255) DEFAULT NULL COMMENT '状态',
     `p_rank` int(11) DEFAULT NULL COMMENT '排序值',
     `p_version` int(11) DEFAULT NULL COMMENT '版本号',
     `p_remark` text DEFAULT NULL COMMENT '备注',

   `log_title` varchar(255) DEFAULT NULL COMMENT '标题',
   `log_content` varchar(255) DEFAULT NULL COMMENT '内容',
   `log_ip` varchar(255) DEFAULT NULL COMMENT 'ip',
     PRIMARY KEY (`p_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- ----------------------------
-- Table structure for azhublog_menu
-- ----------------------------
DROP TABLE IF EXISTS `azhublog_menu`;
CREATE TABLE `azhublog_menu` (
     `p_id` varchar(50) NOT NULL COMMENT 'uuid' ,
     `p_name` varchar(255) DEFAULT NULL COMMENT '名称',
     `p_create_datetime` datetime DEFAULT NULL COMMENT '创建时间',
     `p_update_datetime` datetime DEFAULT NULL COMMENT '修改时间',
     `p_state` varchar(255) DEFAULT NULL COMMENT '状态',
     `p_rank` int(11) DEFAULT NULL COMMENT '排序值',
     `p_version` int(11) DEFAULT NULL COMMENT '版本号',
     `p_remark` text DEFAULT NULL COMMENT '备注',

   `menu_icon` varchar(255) DEFAULT NULL COMMENT '菜单图标',
   `menu_name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
   `menu_target` varchar(255) DEFAULT NULL COMMENT '打开方式',
   `menu_url` varchar(255) DEFAULT NULL COMMENT '菜单路径',
     PRIMARY KEY (`p_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- ----------------------------
-- Table structure for azhublog_options
-- ----------------------------
DROP TABLE IF EXISTS `azhublog_options`;
CREATE TABLE `azhublog_options` (
    `p_id` varchar(50) NOT NULL COMMENT 'uuid' ,
    `p_name` varchar(255) DEFAULT NULL COMMENT '名称',
    `p_create_datetime` datetime DEFAULT NULL COMMENT '创建时间',
    `p_update_datetime` datetime DEFAULT NULL COMMENT '修改时间',
    `p_state` varchar(255) DEFAULT NULL COMMENT '状态',
    `p_rank` int(11) DEFAULT NULL COMMENT '排序值',
    `p_version` int(11) DEFAULT NULL COMMENT '版本号',
    `p_remark` text DEFAULT NULL COMMENT '备注',

      `option_name` varchar(255) NOT NULL COMMENT '设置名',
      `option_value` longtext COMMENT '设置内容',
      PRIMARY KEY (`option_name`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- ----------------------------
-- Table structure for azhublog_tag
-- ----------------------------
DROP TABLE IF EXISTS `azhublog_tag`;
CREATE TABLE `azhublog_tag` (
    `p_id` varchar(50) NOT NULL COMMENT 'uuid' ,
    `p_name` varchar(255) DEFAULT NULL COMMENT '名称',
    `p_create_datetime` datetime DEFAULT NULL COMMENT '创建时间',
    `p_update_datetime` datetime DEFAULT NULL COMMENT '修改时间',
    `p_state` varchar(255) DEFAULT NULL COMMENT '状态',
    `p_rank` int(11) DEFAULT NULL COMMENT '排序值',
    `p_version` int(11) DEFAULT NULL COMMENT '版本号',
    `p_remark` text DEFAULT NULL COMMENT '备注',

  `tag_name` varchar(255) DEFAULT NULL COMMENT '标签名称',
  `tag_url` varchar(255) DEFAULT NULL COMMENT '标签路径',
    PRIMARY KEY (`p_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- ----------------------------
-- Table structure for azhublog_theme
-- ----------------------------
DROP TABLE IF EXISTS `azhublog_theme`;
CREATE TABLE `azhublog_theme` (
      `p_id` varchar(50) NOT NULL COMMENT 'uuid' ,
      `p_name` varchar(255) DEFAULT NULL COMMENT '名称',
      `p_create_datetime` datetime DEFAULT NULL COMMENT '创建时间',
      `p_update_datetime` datetime DEFAULT NULL COMMENT '修改时间',
      `p_state` varchar(255) DEFAULT NULL COMMENT '状态',
      `p_rank` int(11) DEFAULT NULL COMMENT '排序值',
      `p_version` int(11) DEFAULT NULL COMMENT '版本号',
      `p_remark` text DEFAULT NULL COMMENT '备注',

    `theme_name` varchar(255) DEFAULT NULL COMMENT '主题名(url)',
    `theme_describe` varchar(255) DEFAULT NULL COMMENT '主题描述',
    `theme_img` varchar(255) DEFAULT NULL COMMENT '主题预览图',
    `theme_status` int(11) DEFAULT '0' COMMENT '0未启用1已启用',
      PRIMARY KEY (`p_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;

-- ----------------------------
-- Table structure for azhublog_user
-- ----------------------------
DROP TABLE IF EXISTS `azhublog_user`;
CREATE TABLE `azhublog_user` (
     `p_id` varchar(50) NOT NULL COMMENT 'uuid' ,
     `p_name` varchar(255) DEFAULT NULL COMMENT '名称',
     `p_create_datetime` datetime DEFAULT NULL COMMENT '创建时间',
     `p_update_datetime` datetime DEFAULT NULL COMMENT '修改时间',
     `p_state` varchar(255) DEFAULT NULL COMMENT '状态',
     `p_rank` int(11) DEFAULT NULL COMMENT '排序值',
     `p_version` int(11) DEFAULT NULL COMMENT '版本号',
     `p_remark` text DEFAULT NULL COMMENT '备注',

   `login_enable` varchar(255) DEFAULT '0' COMMENT '是否禁用登录',
   `login_error_count` int(11) DEFAULT NULL COMMENT '登录失败次数',
   `login_last_time` datetime DEFAULT NULL COMMENT '最后登录时间',

   `user_portrait` varchar(255) DEFAULT NULL COMMENT '头像',
   `user_explain` varchar(255) DEFAULT NULL COMMENT '说明',
   `user_nickname` varchar(255) DEFAULT NULL COMMENT '用户昵称',
   `user_email` varchar(255) DEFAULT NULL COMMENT '邮箱',
   `user_name` varchar(255) DEFAULT NULL COMMENT '用户名',
   `user_password` varchar(255) DEFAULT NULL COMMENT '密码',
     PRIMARY KEY (`p_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci;
